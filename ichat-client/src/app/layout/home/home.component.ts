import { Component, OnInit, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../../shared/service/user.service';
import { RxStompService } from '@stomp/ng2-stompjs';
import { SocialAuthService } from 'angularx-social-login';
import { TokenStorageService } from 'src/app/shared/service/token-storage.service';
import { AuthService } from 'src/app/shared/service/auth.service';
import { User } from 'src/app/shared/model/user';

@Component({
    selector: 'wt-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

    receiver: string;
    

    constructor(private router: Router, private authService: AuthService
        , private stompService: RxStompService,private tokenStorageService: TokenStorageService ) {
    }

   

    onReceiverChange(event) {
        this.receiver = event;
    }


    clearSession() {
       // sessionStorage.removeItem('user');
        this.stompService.deactivate();
        
       
    }

    private roles!: string[];
    isLoggedIn = false;
    username: string ='';
  
 
  
    ngOnInit(): void {
      
      this.isLoggedIn = !!this.tokenStorageService.getToken();
  
      if (this.isLoggedIn) {
        const user = this.tokenStorageService.getUser();
        this.roles = user.roles;
        this.username = user.displayName;
      }
    }
  

    

}


