import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RxStompService } from '@stomp/ng2-stompjs';

import { User } from './shared/model/user';
import { AuthService } from './shared/service/auth.service';
import { TokenStorageService } from './shared/service/token-storage.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  private roles!: string[];
  isLoggedIn = false;
  showAdminBoard = false;
  showModeratorBoard = false;
  username: string ='';
   
  

  constructor(private router: Router, private authService: AuthService
    , private stompService: RxStompService,private tokenStorageService: TokenStorageService ) { }

  ngOnInit(): void {
    
    this.isLoggedIn = !!this.tokenStorageService.getToken();

    if (this.isLoggedIn) {
      const user = this.tokenStorageService.getUser();

      this.username = user.displayName;
     
    }
  }
  clearSession() {
    // sessionStorage.removeItem('user');
     this.stompService.deactivate();
     this.username = null;
    // this.router.navigate(['/']);
 }

  logout(): void {
    console.log(this.username)
    this.authService.logout(this.username).subscribe()
    this.tokenStorageService.signOut();
    
    this.clearSession();
    window.location.reload();
  }
}
