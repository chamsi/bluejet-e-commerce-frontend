import { Injectable } from '@angular/core';
import { User } from '../model/user';
import { settings } from '../util/settings';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AppConstants } from 'src/app/common/app.constants';


const headers = new HttpHeaders({
    'Content-Type': 'application/json'
});

@Injectable()
export class UserService {

    constructor(private http: HttpClient) { }

  /*  login(user: User): any {
        return this.http.post(settings.baseUrl + '/login', user, { headers: headers });
    }*/
    getCurrentUser(): Observable<any> {
        return this.http.get(AppConstants.API_URL + 'user/me',{headers:headers});
      }

    logout(user: { email: any;}): any {
        return this.http.post(AppConstants.API_URL+ 'auth/logout', {
            email: user.email,
          }, { headers: headers });
    }
    

    findUsers() {
        return this.http.get(AppConstants.API_URL + 'listUsers', { headers: headers });
    }
}
