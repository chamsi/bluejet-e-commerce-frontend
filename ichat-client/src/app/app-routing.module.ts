import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisterComponent } from './layout/register/register.component';

const routes: Routes = [

  { path: 'login',loadChildren: () => import('./login/login.module').then(x => x.LoginModule) },
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'register', component: RegisterComponent },
  { path: 'home',loadChildren: () => import('./layout/home/home.module').then(x => x.HomeModule)}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
